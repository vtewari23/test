import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './Views/Header';
import HomePage from './Views/HomePage';
import LoginPage from './Views/LoginPage';
import SettingsPage from './Views/Settings';
import AboutPage from './Views/About';
import ProfilePage from './Views/Profile';

const App = () => {
  return (
      <Router>
        <div>
          <Header />
            fghjk
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/login" component={LoginPage} />
            <Route path="/settings" component={SettingsPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/profile" component={ProfilePage} />
          </Switch>
        </div>
      </Router>
  );
};

export default App;
