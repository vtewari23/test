import React, { useState } from 'react';

const Dropdown = () => {
    const [selectedOption, setSelectedOption] = useState('');

    const handleOptionChange = (event) => {
        setSelectedOption(event.target.value);
    };

    return (
        <div>
            <label htmlFor="dropdown">Select a question:</label>
            <select id="dropdown" value={selectedOption} onChange={handleOptionChange}>
                <option value="">-- Select --</option>
                <option value="question1">Question 1</option>
                <option value="question2">Question 2</option>
                <option value="question3">Question 3</option>
                {/* Add more options as needed */}
            </select>
        </div>
    );
};

export default Dropdown;
