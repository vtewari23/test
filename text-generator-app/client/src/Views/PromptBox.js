import React from 'react';

const PromptBox = ({ prompt }) => {
    return (
        <div className="prompt-box">
            <h2>Generated Prompt:</h2>
            <p>{prompt}</p>
        </div>
    );
};

export default PromptBox;
