import React from 'react';

const About = () => {
  return (
    <div className="container">
      <div className="form-container">
        <h1>About Us</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
          posuere massa vel tincidunt vestibulum. Phasellus euismod consequat
          nisl a lobortis. Morbi posuere purus id nisi semper tincidunt. Nam
          tincidunt leo a ipsum finibus, sed feugiat metus lacinia. Nulla
          facilisi. Sed feugiat risus arcu, vel suscipit neque porttitor
          auctor. Ut sed sollicitudin lectus, in auctor ligula. Quisque aliquet
          est at metus tincidunt, ut volutpat turpis luctus. Nulla facilisi.
          Quisque a purus et sem convallis iaculis. Curabitur eget dui a est
          tincidunt efficitur vitae ut nulla. Nullam fermentum commodo velit ut
          volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et
          ultrices posuere cubilia curae; Proin tempor neque est, ut aliquet
          ipsum fringilla in.
        </p>
      </div>
      <div className="overlay-container">
        <div className="overlay">
          <div className="overlay-panel overlay-left">
            <h1>Welcome Back</h1>
            <p>
              To keep connected with us, please login with your personal info.
            </p>
            <button className="ghost" id="signIn">Sign In</button>
          </div>
          <div className="overlay-panel overlay-right">
            <h1>Hello, Friend</h1>
            <p>
              Enter your personal details and start your journey with us.
            </p>
            <button className="ghost" id="signUp">Sign Up</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
