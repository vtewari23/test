const DropdownMenu = () => {
  const options = [
    { value: 'option1', label: 'Option 1' },
    { value: 'option2', label: 'Option 2' },
    { value: 'option3', label: 'Option 3' },
    // Add more options as needed
  ];

  return (
    <div>
      <h1>Dropdown Menu</h1>
      <div className="dropdown">
        <select>
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}

ReactDOM.render(<DropdownMenu />, document.getElementById('root'));
